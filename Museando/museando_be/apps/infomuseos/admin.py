from django.contrib import admin
from apps.infomuseos.models import InfoMuseos

class InfoMuseosAdmin(admin.ModelAdmin):
    list_display=('id','name')

admin.site.register(InfoMuseos,InfoMuseosAdmin)

