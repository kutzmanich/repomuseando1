from django.db import models
#from simple_history.models import HistoricalRecords

class InfoMuseos(models.Model): 
   
    id = models.AutoField(primary_key = True)
    name = models.CharField('Nombre del museo', max_length=150, unique = True,blank = False,null = False)
    description = models.CharField('Descripción de museo',max_length=500,blank = False,null = False)
    image = models.ImageField('Imagen del museo', upload_to='infomuseos.media',blank = True,null = True)
    adress = models.CharField('Dirección del museo', max_length=150, unique = True,blank = False,null = False)
    city = models.CharField('Ciudad del museo', max_length=30,blank = False,null = False)
    telephone = models.CharField('Teléfono del museo', max_length=20, unique = True,blank = False,null = False)
    museum_hours = models.CharField('Horario del museo', max_length=150, blank = False,null = False)
    email = models.EmailField('Correo electronico del museo', unique = True,blank = False,null = False) 
    museum_url = models.URLField('URL del museo',blank = True,null = True)
    museum_facebook = models.URLField('Facebook del museo',blank = True,null = True)
    museum_twitter = models.URLField('Twitter del museo',blank = True,null = True)
    museum_instagram = models.URLField('Instagram del museo',blank = True,null = True)
    state = models.BooleanField('Estado',default = True)
    created_date = models.DateField('Fecha de Creación', auto_now=False, auto_now_add=True)
    modified_date = models.DateField('Fecha de Modificación', auto_now=True, auto_now_add=False)
    deleted_date = models.DateField('Fecha de Eliminación', auto_now=True, auto_now_add=False)
    #historical = HistoricalRecords()

    # @property
    # def _history_user(self):
    #     return self.changed_by

    # @_history_user.setter
    # def _history_user(self, value):
    #     self.changed_by = value


