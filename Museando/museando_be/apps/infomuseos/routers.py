from rest_framework.routers import DefaultRouter
from apps.infomuseos.views import InfoMuseosViewSet

router = DefaultRouter()
router.register(r'',InfoMuseosViewSet,basename = 'infomuseos')

urlpatterns = router.urls