from rest_framework import serializers
from apps.infomuseos.models import InfoMuseos

class InfoMuseosSerializer(serializers.ModelSerializer):

    class Meta:
        model = InfoMuseos
        exclude = ('state','created_date','modified_date','deleted_date')
    
    def to_representation(self,instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'description': instance.description,
            'image': instance.image if instance.image != '' else '',
            'adress': instance.adress,
            'city': instance.city,
            'telephone': instance.telephone,
            'museum_hours': instance.museum_hours,
            'email': instance.description,
            'museum_url': instance.museum_url,
            'museum_facebook': instance.museum_facebook,
            'museum_twitter': instance.museum_twitter,
            'museum_instagram': instance.museum_instagram,
        }
