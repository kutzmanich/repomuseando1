from rest_framework import viewsets
from apps.users.serializers import CustomUserSerializer
from apps.users.models import User
from rest_framework.response import Response
from rest_framework import status

class UserViewSet(viewsets.GenericViewSet):
    model = User
    serializer_class = CustomUserSerializer
    queryset = User.objects.all()
    
    def list(self, request):
        product_serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response(product_serializer.data, status=status.HTTP_200_OK)